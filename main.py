import pygame
import random
import math

# Ініціалізація Pygame
pygame.init()

# Розміри вікна гри
window_width = 800
window_height = 600

# Налаштування кольорів
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
blue = (0, 0, 255)
green = (0, 255, 0)

# Створення вікна гри
screen = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Тир")

# Налаштування FPS
clock = pygame.time.Clock()
fps = 60

# Основні змінні гри
score = 0
misses = 0
target_radius = 30
bullet_speed = 10
gun_length = 50
target_speed = 1
target_timer = 0
max_misses = 3
target_lifetime = 2000  # у мілісекундах

# Список куль
bullets = []


# Функція для малювання мішені з кільцями
def draw_target(x, y):
    pygame.draw.circle(screen, black, (x, y), target_radius + 10)
    pygame.draw.circle(screen, white, (x, y), target_radius + 5)
    pygame.draw.circle(screen, black, (x, y), target_radius)
    pygame.draw.circle(screen, red, (x, y), target_radius - 5)
    pygame.draw.circle(screen, white, (x, y), target_radius - 10)


# Функція для малювання зброї
def draw_gun(x, y, angle):
    end_x = x + gun_length * math.cos(angle)
    end_y = y + gun_length * math.sin(angle)
    pygame.draw.line(screen, black, (x, y), (end_x, end_y), 5)


# Функція для обробки руху куль
def move_bullets():
    global score, misses, target_x, target_y, target_timer
    for bullet in bullets[:]:
        bullet['x'] += bullet['vx']
        bullet['y'] += bullet['vy']

        # Перевірка на влучання в мішень
        if (bullet['x'] - target_x) ** 2 + (bullet['y'] - target_y) ** 2 <= target_radius ** 2:
            score += 1
            target_x = random.randint(target_radius, window_width - target_radius)
            target_y = random.randint(target_radius, window_height - target_radius)
            bullets.remove(bullet)
            target_timer = pygame.time.get_ticks()

        # Видалення кулі, якщо вона виходить за межі екрану
        elif bullet['x'] < 0 or bullet['x'] > window_width or bullet['y'] < 0 or bullet['y'] > window_height:
            bullets.remove(bullet)
            misses += 1


# Функція для оновлення позиції мішені
def move_target():
    global target_x, target_y
    target_x += random.choice([-1, 1]) * target_speed
    target_y += random.choice([-1, 1]) * target_speed
    if target_x < target_radius or target_x > window_width - target_radius:
        target_x = random.randint(target_radius, window_width - target_radius)
    if target_y < target_radius or target_y > window_height - target_radius:
        target_y = random.randint(target_radius, window_height - target_radius)


# Функція для відображення стартового екрану
def show_start_screen():
    screen.fill(white)
    font = pygame.font.SysFont(None, 72)
    text = font.render("Тир", True, black)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 - text.get_height() // 2))
    font = pygame.font.SysFont(None, 36)
    text = font.render("Натисніть будь-яку клавішу, щоб почати", True, black)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 + text.get_height()))
    pygame.display.flip()
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                waiting = False


# Функція для відображення екрану геймоверу
def show_game_over_screen():
    screen.fill(white)
    font = pygame.font.SysFont(None, 72)
    text = font.render("Гра завершена", True, black)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 - text.get_height() // 2))
    font = pygame.font.SysFont(None, 36)
    text = font.render(f"Ваш рахунок: {score}", True, black)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 + text.get_height()))
    text = font.render(f"Кількість промахів: {misses}", True, red)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 + 2 * text.get_height()))
    text = font.render("Натисніть будь-яку клавішу, щоб почати знову", True, black)
    screen.blit(text, (window_width // 2 - text.get_width() // 2, window_height // 2 + 3 * text.get_height()))
    pygame.display.flip()
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                waiting = False


# Головний цикл гри
running = True
while True:
    # Відображення стартового екрану
    show_start_screen()

    # Ініціалізація змінних гри
    score = 0
    misses = 0
    bullets = []
    target_x = random.randint(target_radius, window_width - target_radius)
    target_y = random.randint(target_radius, window_height - target_radius)
    target_timer = pygame.time.get_ticks()

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                angle = math.atan2(mouse_y - window_height // 2, mouse_x - window_width // 2)
                vx = bullet_speed * math.cos(angle)
                vy = bullet_speed * math.sin(angle)
                bullets.append({'x': window_width // 2 + gun_length * math.cos(angle),
                                'y': window_height // 2 + gun_length * math.sin(angle),
                                'vx': vx, 'vy': vy})

        # Перевірка кількості промахів
        if misses >= max_misses:
            running = False

        screen.fill(white)

        # Отримання позиції миші та розрахунок кута повороту зброї
        mouse_x, mouse_y = pygame.mouse.get_pos()
        angle = math.atan2(mouse_y - window_height // 2, mouse_x - window_width // 2)

        # Оновлення позиції мішені
        move_target()

        # Перевірка на зникнення мішені
        if pygame.time.get_ticks() - target_timer > target_lifetime:
            target_x = random.randint(target_radius, window_width - target_radius)
            target_y = random.randint(target_radius, window_height - target_radius)
            target_timer = pygame.time.get_ticks()

        # Відображення мішені
        draw_target(target_x, target_y)

        # Відображення зброї
        draw_gun(window_width // 2, window_height // 2, angle)

        # Відображення куль
        for bullet in bullets:
            pygame.draw.circle(screen, black, (int(bullet['x']), int(bullet['y'])), 5)

        # Рух куль
        move_bullets()

        # Відображення рахунку та кількості промахів
        font = pygame.font.SysFont(None, 36)
        text_score = font.render(f"Score: {score}", True, black)
        text_misses = font.render(f"Misses: {misses}", True, red)
        screen.blit(text_score, (10, 10))
        screen.blit(text_misses, (10, 50))

        pygame.display.flip()
        clock.tick(fps)

    # Відображення екрану геймоверу
    show_game_over_screen()
